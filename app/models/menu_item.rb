class MenuItem < ApplicationRecord
  has_many :order_items
  has_many :orders, through: :order_items

  def update_inventory(count)
    update_columns(inventory: count)
  end
end
