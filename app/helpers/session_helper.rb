module SessionHelper
  def log_in(user)
    session[:employee_id] = user.id
  end
  def current_emp
    @current_user ||= Employee.find_by(id: session[:employee_id])
  end
end
def log_out
  session.delete(:employee_id)
  @current_user = nil
end
def logged_in?
  !current_emp.nil?
end