// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
= require turbolinks
//= require_tree .
$(function(){
    $("#order_order_items_attributes_0_menu_item_id").change(function(){
        console.log($(this).val());
        var item_sel = $(this).val();
        $.getJSON(window.location.href + "/line_calc", {menu_item_id: item_sel}, function(data){
            $("#subt").text("$" + data.line_total);
        })
    });
});
$(function(){
    $("#order_item_quantity").change(function(){
        console.log($(this).val());
        var item_sel = $(this).val();
        $.getJSON(window.location.href + "/line_calc", {menu_item_id: item_sel}, function(data){
            $("#subt").text("$" + data.line_total);
        })
    });
});