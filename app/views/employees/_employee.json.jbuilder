json.extract! employee, :id, :name, :position, :password, :payscale, :created_at, :updated_at
json.url employee_url(employee, format: :json)
