class SessionController < ApplicationController
  def new
    @employee = Employee.new
    render 'new'
  end

def create
  user = Employee.find_by(id: params[:session][:employee_id])
    if user && user.password == params[:session][:password]
      log_in(user)
      redirect_to root_url
    else
      render 'new'
    end
  end

def destroy
  log_out
  redirect_to root_url
end

end