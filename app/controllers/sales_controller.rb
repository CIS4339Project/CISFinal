class SalesController < ApplicationController
  def new
  end

  @@start_d = Date.today
  @@end_d = Date.today()



  def total
    respond_to do |format|
      st = params[:shop]
      if !st.nil?
      @@start_d = Date.new st["start_date(1i)"].to_i, st["start_date(2i)"].to_i, st["start_date(3i)"].to_i
      @@end_d = Date.new st["end_date(1i)"].to_i, st["end_date(2i)"].to_i, st["end_date(3i)"].to_i
      end
      sales = sales_range(@@start_d, @@end_d)
      format.html
      format.json {render json: sales}
    end
  end
end
