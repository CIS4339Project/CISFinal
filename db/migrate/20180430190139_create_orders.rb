class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :customer
      t.float :subtotal
      t.float :tax
      t.float :total
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
