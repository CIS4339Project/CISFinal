json.extract! menu_item, :id, :name, :price, :inventory, :created_at, :updated_at
json.url menu_item_url(menu_item, format: :json)
